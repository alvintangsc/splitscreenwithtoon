#region File Description
//-----------------------------------------------------------------------------
// SplitScreenGame.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#endregion

namespace SplitScreenSample
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class SplitScreenGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;

        // We use SpriteBatch to draw a dividing line between our viewports to make it
        // easier to visualize.
        SpriteBatch spriteBatch;
        Texture2D blank;

        // Define the viewports that we wish to render to. We will draw two viewports:
        // - The top half of the screen
        // - The bottom half of the screen
        Viewport playerOneViewport;
        Viewport playerTwoViewport;

        // Each viewport will need a different view and projection matrix in
        // order for them to render the scene from different cameras.
        Matrix playerOneView, playerOneProjection;
        Matrix playerTwoView, playerTwoProjection;

        // We're leveraging the Tank class from the Simple Animation Sample to help
        // illustrate the effect of multiple viewports.
        Tank tank;


        #region Fields for toon shader

        Random random = new Random();


        // Effect used to apply the edge detection and pencil sketch postprocessing.
        Effect postprocessEffect;


        // Overlay texture containing the pencil sketch stroke pattern.
        Texture2D sketchTexture;


        // Randomly offsets the sketch pattern to create a hand-drawn animation effect.
        Vector2 sketchJitter;
        TimeSpan timeToNextJitter;


        // Custom rendertargets.
        RenderTarget2D sceneRenderTarget;
        RenderTarget2D normalDepthRenderTarget;


        // Choose what display settings to use.
        NonPhotoRealisticSettings Settings
        {
            get { return NonPhotoRealisticSettings.PresetSettings[settingsIndex]; }
        }

        int settingsIndex = 0;

        // Current and previous input states.
        KeyboardState lastKeyboardState;
        GamePadState lastGamePadState;
        KeyboardState currentKeyboardState;
        GamePadState currentGamePadState;
        #endregion 

        public SplitScreenGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferWidth = 720;

#if WINDOWS_PHONE
            graphics.IsFullScreen = true;
            TargetElapsedTime = TimeSpan.FromTicks(333333);
#endif
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create and load our tank
            tank = new Tank();
            tank.Load(Content);
            
            // Create the SpriteBatch and texture we'll use to draw our viewport edges.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            blank = new Texture2D(GraphicsDevice, 1, 1);
            blank.SetData(new[] { Color.White });

            // Create the viewports
            playerOneViewport = new Viewport
            {
                MinDepth = 0,
                MaxDepth = 1,
                X = 0,
                Y = 0,
                Width = GraphicsDevice.Viewport.Width,
                Height = GraphicsDevice.Viewport.Height / 2,
            };
            playerTwoViewport = new Viewport
            {
                MinDepth = 0,
                MaxDepth = 1,
                X = 0,
                Y = GraphicsDevice.Viewport.Height / 2,
                Width = GraphicsDevice.Viewport.Width,
                Height = GraphicsDevice.Viewport.Height / 2,
            };

            // Create the view and projection matrix for each of the viewports
            playerOneView = Matrix.CreateLookAt(
                new Vector3(400f, 900f, 200f), 
                new Vector3(-100f, 0f, 0f), 
                Vector3.Up);
            playerOneProjection = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.PiOver4, playerOneViewport.AspectRatio, 10f, 5000f);

            playerTwoView = Matrix.CreateLookAt(
                new Vector3(0f, 800f, 800f), 
                Vector3.Zero, 
                Vector3.Up);
            playerTwoProjection = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.PiOver4, playerTwoViewport.AspectRatio, 10f, 5000f);


            postprocessEffect = Content.Load<Effect>("PostprocessEffect");
            sketchTexture = Content.Load<Texture2D>("SketchTexture");

            
            // Create two custom rendertargets.
            PresentationParameters pp = graphics.GraphicsDevice.PresentationParameters;

            sceneRenderTarget = new RenderTarget2D(graphics.GraphicsDevice,
                                                   pp.BackBufferWidth, pp.BackBufferHeight, false,
                                                   pp.BackBufferFormat, pp.DepthStencilFormat);

            normalDepthRenderTarget = new RenderTarget2D(graphics.GraphicsDevice,
                                                         pp.BackBufferWidth, pp.BackBufferHeight, false,
                                                         pp.BackBufferFormat, pp.DepthStencilFormat);

        }

        protected override void UnloadContent()
        {
            if (sceneRenderTarget != null)
            {
                sceneRenderTarget.Dispose();
                sceneRenderTarget = null;
            }

            if (normalDepthRenderTarget != null)
            {
                normalDepthRenderTarget.Dispose();
                normalDepthRenderTarget = null;
            }
            base.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //KeyboardState keyState = Keyboard.GetState();
            //GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);

            lastKeyboardState = currentKeyboardState;
            lastGamePadState = currentGamePadState;

            currentKeyboardState = Keyboard.GetState();
            currentGamePadState = GamePad.GetState(PlayerIndex.One);


            // Allows the game to exit
            if (currentKeyboardState.IsKeyDown(Keys.Escape) || currentGamePadState.Buttons.Back == ButtonState.Pressed)
            {
                Exit();
            }

            // Switch to the next settings preset?
            if ((currentGamePadState.Buttons.A == ButtonState.Pressed &&
                 lastGamePadState.Buttons.A != ButtonState.Pressed) ||
                (currentKeyboardState.IsKeyDown(Keys.A) &&
                 lastKeyboardState.IsKeyUp(Keys.A)))
            {
                settingsIndex = (settingsIndex + 1) %
                                NonPhotoRealisticSettings.PresetSettings.Length;
            }
            
            float time = (float)gameTime.TotalGameTime.TotalSeconds;

            // We animate the tank just like the Simple Animation Sample, to show that the same
            // object really is being drawn on both viewports.
            tank.WheelRotation = time * 5;
            tank.SteerRotation = (float)Math.Sin(time * 0.75f) * 0.5f;
            tank.TurretRotation = (float)Math.Sin(time * 0.333f) * 1.25f;
            tank.CannonRotation = (float)Math.Sin(time * 0.25f) * 0.333f - 0.333f;
            tank.HatchRotation = MathHelper.Clamp((float)Math.Sin(time * 2) * 2, -1, 0);

            // We'll rotate our player two around the tank
            playerTwoView = Matrix.CreateLookAt(
                new Vector3((float)Math.Cos(time), 1f, (float)Math.Sin(time)) * 800f,
                Vector3.Zero,
                Vector3.Up);


            #region Toon Shader Updates
            // Update the sketch overlay texture jitter animation.
            if (Settings.SketchJitterSpeed > 0)
            {
                timeToNextJitter -= gameTime.ElapsedGameTime;

                if (timeToNextJitter <= TimeSpan.Zero)
                {
                    sketchJitter.X = (float)random.NextDouble();
                    sketchJitter.Y = (float)random.NextDouble();

                    timeToNextJitter += TimeSpan.FromSeconds(Settings.SketchJitterSpeed);
                }
            }
            #endregion


            base.Update(gameTime);
        }
        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // Clear the backbuffer. We don't clear inside our DrawScene method because Clear is based
            // on the backbuffer and not the current viewport. This means that if we called Clear for
            // each viewport, only the last one would appear on screen.
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // Here we want to reset some render states our 3D rendering needs that our SpriteBatch changed
            GraphicsDevice.BlendState = BlendState.Opaque;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;



            #region Toon shading
            // If we are doing edge detection, first off we need to render the
            // normals and depth of our model into a special rendertarget.
            if (Settings.EnableEdgeDetect)
            {
                GraphicsDevice.SetRenderTarget(normalDepthRenderTarget);

                GraphicsDevice.Clear(Color.Black);
                // Draw our scene with all of our viewports and their respective view/projection matrices.
                DrawScene(gameTime, playerOneViewport, playerOneView, playerOneProjection, "NormalDepth");
                DrawScene(gameTime, playerTwoViewport, playerTwoView, playerTwoProjection, "NormalDepth");
               // DrawModel(rotation, view, projection, "NormalDepth");
            }

            // If we are doing edge detection and/or pencil sketch processing, we
            // need to draw the model into a special rendertarget which can then be
            // fed into the postprocessing shader. Otherwise can just draw it
            // directly onto the backbuffer.
            if (Settings.EnableEdgeDetect || Settings.EnableSketch)
                GraphicsDevice.SetRenderTarget(sceneRenderTarget);
            else
                GraphicsDevice.SetRenderTarget(null);

            GraphicsDevice.Clear(Color.CornflowerBlue);

            // Draw the model, using either the cartoon or lambert shading technique.
            string effectTechniqueName;

            if (Settings.EnableToonShading)
                effectTechniqueName = "Toon";
            else
                effectTechniqueName = "Lambert";
            DrawScene(gameTime, playerOneViewport, playerOneView, playerOneProjection, effectTechniqueName);
            DrawScene(gameTime, playerTwoViewport, playerTwoView, playerTwoProjection, effectTechniqueName);
           // DrawModel(rotation, view, projection, effectTechniqueName);

            // Run the postprocessing filter over the scene that we just rendered.
            if (Settings.EnableEdgeDetect || Settings.EnableSketch)
            {
                GraphicsDevice.SetRenderTarget(null);

                ApplyPostprocess();
            }


            #endregion




            //// Draw our scene with all of our viewports and their respective view/projection matrices.
            //DrawScene(gameTime, playerOneViewport, playerOneView, playerOneProjection);
            //DrawScene(gameTime, playerTwoViewport, playerTwoView, playerTwoProjection);

            //// Now we'll draw the viewport edges on top so we can visualize the viewports more easily.
            DrawViewportEdges(playerOneViewport);
            DrawViewportEdges(playerTwoViewport);

            base.Draw(gameTime);
        }

        /// <summary>
        /// DrawScene is our main rendering method. By rendering the entire scene inside of this method,
        /// we enable ourselves to be able to render the scene using any viewport we may want.
        /// </summary>
        private void DrawScene(GameTime gameTime, Viewport viewport, Matrix view, Matrix projection, string technique)
        {


            // Set our viewport. We store the old viewport so we can restore it when we're done in case
            // we want to render to the full viewport at some point.
            Viewport oldViewport = GraphicsDevice.Viewport;
            GraphicsDevice.Viewport = viewport;
            
            // Here we'd want to draw our entire scene. For this sample, that's just the tank.
            tank.Draw(Matrix.Identity, view, projection, technique);

            // Now that we're done, set our old viewport back on the device
            GraphicsDevice.Viewport = oldViewport;
        }

        /// <summary>
        /// A helper to draw the edges of a viewport.
        /// </summary>
        private void DrawViewportEdges(Viewport viewport)
        {
            const int edgeWidth = 2;

            // We now compute four rectangles that make up our edges
            Rectangle topEdge = new Rectangle(
                viewport.X - edgeWidth / 2,
                viewport.Y - edgeWidth / 2,
                viewport.Width + edgeWidth,
                edgeWidth);
            Rectangle bottomEdge = new Rectangle(
                viewport.X - edgeWidth / 2,
                viewport.Y + viewport.Height - edgeWidth / 2,
                viewport.Width + edgeWidth,
                edgeWidth);
            Rectangle leftEdge = new Rectangle(
                viewport.X - edgeWidth / 2,
                viewport.Y - edgeWidth / 2,
                edgeWidth,
                viewport.Height + edgeWidth);
            Rectangle rightEdge = new Rectangle(
                viewport.X + viewport.Width - edgeWidth / 2,
                viewport.Y - edgeWidth / 2,
                edgeWidth,
                viewport.Height + edgeWidth);

            // We just use SpriteBatch to draw the four rectangles
            spriteBatch.Begin();
            spriteBatch.Draw(blank, topEdge, Color.Black);
            spriteBatch.Draw(blank, bottomEdge, Color.Black);
            spriteBatch.Draw(blank, leftEdge, Color.Black);
            spriteBatch.Draw(blank, rightEdge, Color.Black);
            spriteBatch.End();
        }

        /// <summary>
        /// Helper applies the edge detection and pencil sketch postprocess effect.
        /// </summary>
        void ApplyPostprocess()
        {
            EffectParameterCollection parameters = postprocessEffect.Parameters;
            string effectTechniqueName;

            // Set effect parameters controlling the pencil sketch effect.
            if (Settings.EnableSketch)
            {
                parameters["SketchThreshold"].SetValue(Settings.SketchThreshold);
                parameters["SketchBrightness"].SetValue(Settings.SketchBrightness);
                parameters["SketchJitter"].SetValue(sketchJitter);
                parameters["SketchTexture"].SetValue(sketchTexture);
            }

            // Set effect parameters controlling the edge detection effect.
            if (Settings.EnableEdgeDetect)
            {
                Vector2 resolution = new Vector2(sceneRenderTarget.Width,
                                                 sceneRenderTarget.Height);

                Texture2D normalDepthTexture = normalDepthRenderTarget;

                parameters["EdgeWidth"].SetValue(Settings.EdgeWidth);
                parameters["EdgeIntensity"].SetValue(Settings.EdgeIntensity);
                parameters["ScreenResolution"].SetValue(resolution);
                parameters["NormalDepthTexture"].SetValue(normalDepthTexture);

                // Choose which effect technique to use.
                if (Settings.EnableSketch)
                {
                    if (Settings.SketchInColor)
                        effectTechniqueName = "EdgeDetectColorSketch";
                    else
                        effectTechniqueName = "EdgeDetectMonoSketch";
                }
                else
                    effectTechniqueName = "EdgeDetect";
            }
            else
            {
                // If edge detection is off, just pick one of the sketch techniques.
                if (Settings.SketchInColor)
                    effectTechniqueName = "ColorSketch";
                else
                    effectTechniqueName = "MonoSketch";
            }

            // Activate the appropriate effect technique.
            postprocessEffect.CurrentTechnique = postprocessEffect.Techniques[effectTechniqueName];

            // Draw a fullscreen sprite to apply the postprocessing effect.
            spriteBatch.Begin(0, BlendState.Opaque, null, null, null, postprocessEffect);
            spriteBatch.Draw(sceneRenderTarget, Vector2.Zero, Color.White);
            spriteBatch.End();
        }

    }
}
